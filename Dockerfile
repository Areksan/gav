FROM node:17-alpine as builder
RUN mkdir /app
WORKDIR /app
COPY ./package.json /app/
RUN yarn install
COPY . /app
RUN yarn build


FROM nginx:1.21-alpine
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /app/dist/ /usr/share/nginx/html/
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
